import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src')
    }
  },
  server: {
    port: 8080,
    strictPort: false, // Port被占用時直接退出， false會嘗試連接下一個可用Port
    open: true, // dev時自動打開網頁，也可以給網址指定。
    // 自訂代理規則，配合後端進行Api呼叫等。
    proxy: {
      '/api': {
        target: "http://www.opshell/api/", // 本機串接
        ws: true, // 代理的WebSockets
        changeOrigin: true, // 允許websockets跨域
        rewrite: (path) => path.replace(/^\/api/, '')
      },
    }
  }
})
