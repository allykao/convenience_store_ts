# Vue 3 + TypeScript + Vite

將Vue.js實作Google Map API 專案以 TypeScript 重構

## 執行專案

- npm install
- npm run dev

## 資料來源

- 政府資料開放平臺(https://data.gov.tw/dataset/32086)